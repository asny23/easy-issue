export type Issue = {
  id: number
  title: string
  description: string
  poster: string
  type: string
  status: string
  created_at: number
  updated_at: number
}
