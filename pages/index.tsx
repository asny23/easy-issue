import Page from '../components/page'
import { Box, Heading, Paragraph, Anchor } from 'grommet'
import { Bug, Optimize } from 'grommet-icons'
import { Config } from '../config'

const Index = () => (
  <Page title="Home">
    <Box align="center">
      <Heading level="1">{Config.siteName}</Heading>
      <Paragraph>
        Simple issue view &amp; post system<br />
        made with <Anchor href="https://nextjs.org/" target="_blank" rel="noopener noreferrer" label="Next.js" />
      </Paragraph>
      <Anchor href={`${Config.subPath}/bugs`} icon={<Bug />} label="Bugs" margin="small" />
      <Anchor href={`${Config.subPath}/requests`} icon={<Optimize />} label="Requests" margin="small" />
    </Box>
  </Page>
)

export default Index
