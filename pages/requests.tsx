import { Heading, Text } from 'grommet'
import useSWR from 'swr'
import Page from '../components/page'
import IssueBlock from '../components/issueblock'
import FormBlock from '../components/formblock'
import { Api } from '../config'
import fetcher from '../model/fetcher'
import { Issue } from '../model/issue'

const Requests = () => (
  <Page title="Requests">
    <Heading level="2" textAlign="center" margin={{ vertical: 'small', horizontal: 'auto' }}>Requests</Heading>
    {issueTable()}
    <FormBlock type="request" />
  </Page>
)

const issueTable = () => {
  const { data, error } = useSWR<Issue[]>(Api.getRequests, fetcher)
  if (error) return <Text>failed to load</Text>
  if (!data) return <Text>loading...</Text>
  return <IssueBlock issues={data} />
}

export default Requests
