import { Heading, Text } from 'grommet'
import useSWR from 'swr'
import Page from '../components/page'
import IssueBlock from '../components/issueblock'
import FormBlock from '../components/formblock'
import { Api } from '../config'
import fetcher from '../model/fetcher'
import { Issue } from '../model/issue'

const Bugs = () => (
  <Page title="Bugs">
    <Heading level="2" textAlign="center" margin={{ vertical: 'small', horizontal: 'auto' }}>Bugs</Heading>
    {issueTable()}
    <FormBlock type="bug" />
  </Page>
)

const issueTable = () => {
  const { data, error } = useSWR<Issue[]>(Api.getBugs, fetcher)
  if (error) return <Text>failed to load</Text>
  if (!data) return <Text>loading...</Text>
  return <IssueBlock issues={data} />
}

export default Bugs
