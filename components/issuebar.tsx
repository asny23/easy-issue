import * as React from 'react'
import { Box, Paragraph, Text } from 'grommet'

import { Issue } from '../model/issue'

type Props = {
  issue: Issue
}

const IssueBar: React.FunctionComponent<Props> = ({ issue }) => {
  const [ state, setState ] = React.useState({
    expand: false
  })
  const renderDetail = () => {
    if(state.expand) {
      return (
        <Box>
          <Text size="small" weight="bold" className="status-label">{issue.status}</Text>
          <Paragraph margin="small" style={{ maxWidth: '100%', whiteSpace: 'pre-wrap' }}>
            {issue.description}
          </Paragraph>
          {issue.poster.length ? <Text color="#808080" size="small">from {issue.poster}</Text> : undefined}
        </Box>
      )
    }
  }
  const expand = () => {
    setState({ expand: !state.expand })
  }
  return (
    <Box
      margin="xsmall"
      pad="xxsmall"
      key={issue.id}
      className={`issue-bar ${issue.status}`}
      border={{ color: "#A0A0A0", style: "solid" }}
      round="xsmall"
      onClick={() => expand()}
    >
      <Text>{issue.title}</Text>
      {renderDetail()}
    </Box>
  )
}

export default IssueBar
