import * as React from 'react'
import { Box } from 'grommet'

import { Issue } from '../model/issue'
import IssueBar from './issuebar'

type Props = {
  issues: Issue[]
}

const IssueBlock: React.FunctionComponent<Props> = ({ issues }) => (
  <Box width={{ max: '90%' }} margin={{ vertical: 'medium', horizontal: 'auto' }}>
    {issues.map(issue => <IssueBar key={issue.id} issue={issue} />)}
  </Box>
)

export default IssueBlock
