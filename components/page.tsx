import * as React from 'react'
import Head from 'next/head'
import { Grommet, Header, Footer, Grid, Anchor } from 'grommet'
import { Home, Bug, Optimize } from 'grommet-icons'
import { Config } from '../config'

type Props = {
  title?: string
}

const Page: React.FunctionComponent<Props> = ({
  children,
  title = '',
}) => (
  <div>
    <Head>
      <meta charSet="utf-8" />
      <title>{`${title} - ${Config.siteName}`}</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="stylesheet" type="text/css" href={`${Config.subPath}/styles.css`} />
    </Head>
    <Grommet>
      <Header background="#FFF0FA" pad="xsmall">
        <Grid
          margin="auto"
          rows={["fill"]}
          columns={["flex", "flex", "flex"]}
          gap="none"
          areas={[
            { name: 'top', start: [0, 0], end: [0, 0] },
            { name: 'bugs', start: [1, 0], end: [1, 0] },
            { name: 'requests', start: [2, 0], end: [2, 0] }
          ]}
        >
          <Anchor href={`${Config.subPath}/`} icon={<Home />} label="Top" gridArea="top" />
          <Anchor href={`${Config.subPath}/bugs`} icon={<Bug />} label="Bugs" gridArea="bugs" />
          <Anchor href={`${Config.subPath}/requests`} icon={<Optimize />} label="Requests" gridArea="requests" />
        </Grid>
      </Header>
      {children}
      <Footer background="#E0E0E0" margin={{ top: 'medium' }} pad="small" direction="column">
      </Footer>
    </Grommet>
  </div>
)

export default Page
