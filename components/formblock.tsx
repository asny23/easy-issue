import * as React from "react"
import Router from "next/router"
import encoding from "text-encoding"
import { Box, Heading, Table, TableBody, TableCell, TableRow, Text, TextInput, TextArea, Button } from "grommet"
import { FormUpload } from "grommet-icons"
import { Api } from "../config"

type Props = {
  type: string
}

const FormBlock: React.FunctionComponent<Props> = ({ type }) => {
  const [ state, setState ] = React.useState({
    title: '',
    description: '',
    poster: '',
    buttonDisabled: false,
    error: ''
  })
  const formIndex = (type: string) => {
    switch(type) {
      case 'bug':
        return 'バグを報告'
      case 'request':
        return '要望・提案'
      default:
        return 'issueを作成'
    }
  }
  const submit = () => {
    if(!state.title.length || !state.description.length) {
      setState({ ...state, error: '件名と内容を入力してください' })
      return
    }
    setState({ ...state, buttonDisabled: true })
    fetch(
      Api.postIssues,
      {
        method: 'POST',
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({
          data: {
            id: 'INCREMENT',
            title: state.title,
            description: state.description,
            poster: state.poster,
            type: type,
            status: 'initial',
            created_at: Date.now(),
            updated_at: Date.now()
          }
        })
      }
    ).then(response => {
      if(response.status == 201) {
        Router.reload()
      } else {
        if(response.body) {
          const reader = response.body.getReader()
          const decoder = new encoding.TextDecoder()
          reader.read().then(({ value }) => {
            setState({ ...state, error: decoder.decode(value), buttonDisabled: false })
          })
        }
      }
    })
  }
  return (
    <Box align="center">
      <Heading level="2">{formIndex(type)}</Heading>
      <Table>
        <TableBody>
          <TableRow>
            <TableCell scope="row">件名</TableCell>
            <TableCell>
              <TextInput
                name="title"
                placeholder="件名"
                required={true}
                style={{ fontFamily: "monospace" }}
                onChange={event => setState({ ...state, title: event.target.value })}
              />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell scope="row">内容</TableCell>
            <TableCell>
              <TextArea
                name="description"
                placeholder="くわしく記入してください"
                required={true}
                cols={32} rows={4}
                style={{ fontFamily: "monospace" }}
                onChange={event => setState({ ...state, description: event.target.value })}
              />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell scope="row">お名前</TableCell>
            <TableCell>
              <TextInput name="poster" placeholder="記入は任意です" onChange={event => setState({ ...state, poster: event.target.value })}/>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      <Text color="#C04020">{state.error}</Text>
      <Button label="送信" icon={<FormUpload />} margin="small" onClick={() => submit()} disabled={state.buttonDisabled}/>
    </Box>
  )
}

export default FormBlock
