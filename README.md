# EasyIssue

a simple issue view & post app

made with [Next.js](https://nextjs.org/)

## Demo

- [Demo on Netlify](https://easyissue-demo.netlify.app/)

## Usage

- Clone repository

- Edit `.env` or copy `.env` to `.env.[environment]` ([guide](https://nextjs.org/docs/basic-features/environment-variables#exposing-environment-variables))

- Put your [SheetDB API](https://docs.sheetdb.io/#sheetdb-api) or other API endpoint to `.env.[environment]`
  If you use SheetDB, just set `DOCUMENT_ID` and `SHEET_NAME`

  ```
  DOCUMENT_ID=
  SHEET_NAME=
  NEXT_PUBLIC_SUBPATH=

  NEXT_PUBLIC_API_GETBUGS=https://sheetdb.io/api/v1/$DOCUMENT_ID/search?type=bug&sheet=$SHEET_NAME
  NEXT_PUBLIC_API_GETREQUESTS=https://sheetdb.io/api/v1/$DOCUMENT_ID/search?type=request&sheet=$SHEET_NAME
  NEXT_PUBLIC_API_POSTISSUES=https://sheetdb.io/api/v1/$DOCUMENT_ID/?sheet=$SHEET_NAME
  ```

- If you use SheetDB, fill in the first row of sheet with following values

  `id title description poster type status created_at updated_at`

- If you want to use EasyIssue with subpath, set `NEXT_PUBLIC_SUBPATH` to `.env.[environment]` (e.g. `NEXT_PUBLIC_SUBPATH=/issues`)

- Run locally

  `$ npm install`

  `$ npm run dev`

- Static HTML export to `out` directory

  `$ npm run export`

## Q&A

- Can I edit or delete issue?
  - Can't do it on app. Edit spreadsheet directly.
