export namespace Config {
  export const siteName = 'EasyIssue'
  export const subPath = process.env.NEXT_PUBLIC_SUBPATH ?? ''
}

export namespace Api {
  export const getBugs = process.env.NEXT_PUBLIC_API_GETBUGS ?? ''
  export const getRequests = process.env.NEXT_PUBLIC_API_GETREQUESTS ?? ''
  export const postIssues = process.env.NEXT_PUBLIC_API_POSTISSUES ?? ''
}
